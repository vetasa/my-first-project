package s7;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class FullScreen {
public static void main(String[] args) throws Throwable {
	System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.facebook.com/");
	//Go to full screemode
	Thread.sleep(3000);
	driver.manage().window().fullscreen();
	Thread.sleep(3000);
	driver.manage().window().minimize();
	
	
}
}