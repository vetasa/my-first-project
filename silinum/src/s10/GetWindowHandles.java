package s10;




import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetWindowHandles {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
//	 driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		// create a new window here upon same obj refernce

		driver.switchTo().newWindow(WindowType.WINDOW).get("https://www.instagram.com/");
		
		driver.switchTo().newWindow(WindowType.TAB).get("https://www.amazon.com/");
		// capture all the windows ID
		Set<String> allwindowsId = driver.getWindowHandles();
		// check for how many windows id
		int windowidCount = allwindowsId.size();
		// print the count Of windows id
		System.out.println("windowidCount = " + windowidCount);
		// iterate the set of windows Id
		for (String wid : allwindowsId) {
			//print each window ID
			System.out.println(wid);
			

		}

	}
	
}
