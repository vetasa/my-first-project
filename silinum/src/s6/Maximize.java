package s6;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Maximize {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
		// Instantiate the browser specific class
		WebDriver driver = new ChromeDriver();
		// pre-condition
		// maximize the browser
		// We will get the better view of the application.
		// we are utilizing the browser with it's fullest capacity
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		
	}
}