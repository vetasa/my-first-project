package datadriventesting;

import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;



public class FirstAndLastRowLimitation {
public static void main(String[] args) throws Throwable {

	FileInputStream fis=new FileInputStream("./Resources/facebookdata.xlsx");
	Workbook workbook=WorkbookFactory.create(fis);
Thread.sleep(3000);
	Sheet sheet=workbook.getSheet("doblist");
	int firstRowIndex=sheet.getFirstRowNum();
	System.out.println(firstRowIndex);
	int lastRowIndex=sheet.getLastRowNum();
	System.out.println(lastRowIndex);
	workbook.close();
	
	
}
}
