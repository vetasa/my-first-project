package learnscreenshot;
import java.time.LocalDateTime;
public class TimeStamp {
public static void main(String[] args) {
	LocalDateTime local=LocalDateTime.now();
	System.out.println(local);
	String timeString=local.toString();
	System.out.println(timeString);
	String updatedString=timeString.replace(':', '-');
	System.out.println(updatedString);
	String timeStamp=LocalDateTime.now().toString().replace(':','-');
	System.out.println(timeStamp);
}
}
