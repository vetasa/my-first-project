package fullscreen;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

public class FullScreen {
public static void main(String[] args) throws Throwable {
	System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
	WebDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("https://www.facebook.com/");
	//driver.manage().window().fullscreen();
	driver.switchTo().newWindow(WindowType.WINDOW);
	Thread.sleep(2000);
	Dimension targetWindowSize=new Dimension(250,250);
	driver.manage().window().setSize(targetWindowSize);
	Dimension WindowSize=driver.manage().window().getSize();
	int WindowWidth=WindowSize.getWidth();
	System.out.println("WindowWidth="+WindowWidth);
	
	int WindowHeight=WindowSize.getHeight();
	System.out.println("WindowHeight="+WindowHeight);
	//System.out.println(targetWindowSize);
	//driver.manage().window().minimize();
	driver.quit();
	
	
	
	
}
}
