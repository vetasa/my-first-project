package workonlocators;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ByLocatorClassName {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.manage().window().maximize();
driver.get("https://www.facebook.com/");
WebElement tagLine=driver.findElement(By.className("_8eso"));
String actualTagLineText=tagLine.getText();
System.out.println(actualTagLineText);
String expTagLineText="Faceb-ok helps you connect and share with the people in your life.";
System.out.println(expTagLineText);
if(actualTagLineText.equals(expTagLineText)) {
	System.out.println("LINE TEXT IS FOUND CORRECT AND ITS VERIFIED");
}else
{
	System.out.println("line text is found incorrect and its verified");
}
driver.manage().window().minimize();
driver.quit();
}
}