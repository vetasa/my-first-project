package s9;
import java.util.Set;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetWindowHandle {
public static void main(String[] args) throws Throwable {
	System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
	
	WebDriver driver = new ChromeDriver();

	driver.manage().window().maximize();
	
	driver.get("https://www.facebook.com/");
	Thread.sleep(2000);
	driver.switchTo().newWindow(WindowType.WINDOW).get("https://www.instagram.com/");
	driver.close();
	
	Set<String> p = driver.getWindowHandles();
	System.out.println(p);
	
	
}
}