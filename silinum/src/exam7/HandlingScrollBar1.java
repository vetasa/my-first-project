package exam7;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HandlingScrollBar1 {
public static void main(String[] args) {
	System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
	WebDriver driver=new ChromeDriver();
	driver.get("https://www.bbc.com/");
WebElement y=driver.findElement(By.xpath("//span[text()='Future Planet']"));
	JavascriptExecutor j= (JavascriptExecutor) driver;
	j.executeScript("window.scrollBy(0,"+y+")");
}
}
