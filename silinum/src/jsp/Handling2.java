package jsp;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Handling2 {
public static void main(String[] args) throws InterruptedException {
	System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
	WebDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("file:///D:/pp/page1.html");
	Thread.sleep(4000);
	driver.findElement(By.id("t1")).sendKeys("a");
	driver.switchTo().frame("f2");
	Thread.sleep(4000);
	driver.findElement(By.id("t2")).sendKeys("b");
	driver.switchTo().defaultContent();
	
	driver.findElement(By.id("t1")).sendKeys("c");
	WebElement f=driver.findElement(By.xpath("//iframe"));
	driver.switchTo().frame(f);
	
	driver.findElement(By.id("t2")).sendKeys("d");
	
}
}
