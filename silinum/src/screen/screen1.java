package screen;
import java.io.File;

import java.io.IOException;
import java.time.LocalDateTime;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
public class screen1 {
	public static void main(String[] args) throws IOException, Throwable {
		System.setProperty("webdriver.chrome.driver", "D:\\slinum\\chromedriver_win32\\chromedriver.exe");
WebDriver driver=new ChromeDriver();
driver.manage().window().maximize();
driver.get("https://www.facebook.com/");
//capture the webelement screenshot
//WebElement ts=driver.findElement(By.name("login"));

//explicit type casting
TakesScreenshot ts=(TakesScreenshot)driver; //TakesScreenshot ts1=(TakesScreenshot)driver;
File temp=ts.getScreenshotAs(OutputType.FILE);
String address=temp.getAbsolutePath();
System.out.println(address);
String timeStamp=LocalDateTime.now().toString().replace(':', '-');
File permanent=new File("./errorshots/"+timeStamp+"face.png");
FileUtils.copyFile(temp,permanent);
//Thread.sleep(20000);
	}
} 