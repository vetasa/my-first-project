package com.actime.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserListPage {
	@FindBy(xpath="//div[text()='New User']")
	private WebElement newUser;
	@FindBy(xpath="//input[@id='createUserPanel_firstNameField']")
	private WebElement firstName;
	@FindBy(xpath="//input[@id='createUserPanel_lastNameField']")
	private WebElement lastName;
	@FindBy(xpath="//input[@id='createUserPanel_emailField']")
	private WebElement emailId;
	@FindBy(xpath="(//div[text()='-- department not assigned --'])[1]")
	private WebElement dropDown;
	@FindBy(xpath="(//div[text()='HR & Finance'])[3]")
	private WebElement optHr;
	@FindBy(xpath="//div[text()='Save & Send Invitation']")
	private WebElement savebtn;
	@FindBy(xpath="(//span[text()='Close'])[1]")
	private WebElement close;
	public UserListPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	public void setUser() throws Throwable
	{
		Thread.sleep(5000);
		newUser.click();
		Thread.sleep(5000);
		firstName.sendKeys("murali2");
		Thread.sleep(5000);
		lastName.sendKeys("billa2");
		Thread.sleep(5000);
		emailId.sendKeys("mural199895@gmail.com");
		Thread.sleep(5000);
		dropDown.click();
		Thread.sleep(5000);
		optHr.click();
		Thread.sleep(5000);
		savebtn.click();
		Thread.sleep(5000);
		close.click();
		
	}
	
	

}

