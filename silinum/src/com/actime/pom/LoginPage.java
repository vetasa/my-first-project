package com.actiTime.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.actitime.generic.BaseClass;
import com.actiTime.Generic.FileLib;

public class LoginPage extends BaseClass {
	@FindBy(id="username")
	private WebElement untbx;
	@FindBy(name="pwd")
	private WebElement pwtbx;
	@FindBy(xpath="//div[text()='Login ']")
	private WebElement lgbtn;
	public LoginPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void setLogin(String userName,String password) throws Throwable
	{
		FileLib f=new FileLib();
		untbx.sendKeys(userName);
		pwtbx.sendKeys(password);
		lgbtn.click();
	}
	

}
