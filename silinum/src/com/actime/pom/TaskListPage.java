package com.actime.pom;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class TaskListPage {
	@FindBy(xpath="//div[text()='Add New']")
	private WebElement addNewTask;
	@FindBy(xpath="//div[text()='+ New Customer']")
	private WebElement newCustomer;
	@FindBy(xpath="(//input[@placeholder='Enter Customer Name'])[2]")
	private WebElement PopUpCustomerName;
	@FindBy(xpath="//textarea[@placeholder='Enter Customer Description']")
	private WebElement PopUpDiscription;
	@FindBy(xpath="(//div[text()='- Select Customer -'])[1]")
	private WebElement dropDownCopyProject;
	@FindBy(xpath="(//div[text()='Big Bang Company'])[6]")
	private WebElement selectBinsGroup;
	
	@FindBy(xpath="//div[text()='Create Customer']")
	private WebElement createCustomerBtn;
	public TaskListPage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void setNewCustomer() throws Throwable
	{
		Thread.sleep(5000);
		addNewTask.click();
		Thread.sleep(5000);
		newCustomer.click();
		Thread.sleep(5000);
		PopUpCustomerName.sendKeys("hdfc001");
		Thread.sleep(5000);
		PopUpDiscription.sendKeys("bankingProject");
		Thread.sleep(5000);
		
		Thread.sleep(5000);
		
		dropDownCopyProject.click();		
      	selectBinsGroup.click();
		Thread.sleep(5000);
		createCustomerBtn.click();
		
		
	}
	

}
