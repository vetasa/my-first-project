
package com.actiTime.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.actiTime.Generic.BaseClassOfActiTime;

public class Homepage extends BaseClassOfActiTime {
	@FindBy(xpath="//a[text()='Logout']")
	private WebElement lgout;
	@FindBy(xpath ="//div[@id='container_tasks']")
	private WebElement taskTab;
	@FindBy(id ="container_users")
	private WebElement userTab;
	public Homepage(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	public void setLogout()
	{
	
		lgout.click();
		
	}
	public void setTaskTab()
	{
	
		taskTab.click();
		
	}
	public void setUserTab()
	{
	
		userTab.click();
		
	}
}
